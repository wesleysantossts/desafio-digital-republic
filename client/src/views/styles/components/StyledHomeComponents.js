import styled from "styled-components";

export const BlocoFormulario = styled.div`
  width: 100%;
  display: grid;
  grid-template: auto / auto auto;
  grid-gap: 5px 10px; 
  
  @media(min-width: 1000px){
    width: 100%;
    display: grid;
    grid-template: auto / auto auto auto auto;
    grid-gap: 5px 10px; 
  }
`;