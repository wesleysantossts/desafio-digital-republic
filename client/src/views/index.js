import { useState } from "react";
// import {QtdTintas} from "../contexts/qtdTintas"

import { BlocoFormulario } from "./styles/components/StyledHomeComponents";
import { HomeNavbar, HomeFooter, CalculoMetroQuadrado, CalculoPortasEJanelas, Resultado } from "../components/HomeComponents";
import {IoClose} from "react-icons/io5";
import "./styles/index.css";

export default function Home(){
  const parede = {altura: "", largura: "", portas: "", janelas:""};

  // Paredes
  const [paredeUm, setParedeUm] = useState(parede), [paredeDois, setParedeDois] = useState(parede), [paredeTres, setParedeTres] = useState(parede), [paredeQuatro, setParedeQuatro] = useState(parede), [casa, setCasa] = useState([]);

  function pegarFormulario(e){
    
    try {
      // Tamanho das paredes
      if(e.target.getAttribute("name") === "paredeUmAltura") setParedeUm({"altura": Number(e.target.value), "largura": paredeUm.largura, "portas":paredeUm.portas, "janelas": paredeUm.janelas});
      if(e.target.getAttribute("name") === "paredeUmLargura") setParedeUm({"altura": paredeUm.altura, "largura": Number(e.target.value), "portas":paredeUm.portas, "janelas": paredeUm.janelas});
      if(e.target.getAttribute("name") === "paredeDoisAltura") setParedeDois({"altura": Number(e.target.value), "largura": paredeDois.largura, "portas":paredeDois.portas, "janelas": paredeDois.janelas});
      if(e.target.getAttribute("name") === "paredeDoisLargura") setParedeDois({"altura": paredeDois.altura, "largura": Number(e.target.value), "portas":paredeDois.portas, "janelas": paredeDois.janelas});
      if(e.target.getAttribute("name") === "paredeTresAltura") setParedeTres({"altura": Number(e.target.value), "largura": paredeTres.largura, "portas":paredeTres.portas, "janelas": paredeTres.janelas});
      if(e.target.getAttribute("name") === "paredeTresLargura") setParedeTres({"altura": paredeTres.altura, "largura": Number(e.target.value), "portas":paredeTres.portas, "janelas": paredeTres.janelas});
      if(e.target.getAttribute("name") === "paredeQuatroAltura") setParedeQuatro({"altura": Number(e.target.value), "largura": paredeQuatro.largura, "portas":paredeQuatro.portas, "janelas": paredeQuatro.janelas});
      if(e.target.getAttribute("name") === "paredeQuatroLargura") setParedeQuatro({"altura": paredeQuatro.altura, "largura": Number(e.target.value), "portas":paredeQuatro.portas, "janelas": paredeQuatro.janelas});
      
      // Portas e janelas
      if(e.target.getAttribute("name") === "paredeUmPortas") setParedeUm({"altura": paredeUm.altura, "largura": paredeUm.largura, "portas": Number(e.target.value), "janelas": paredeUm.janelas});
      if(e.target.getAttribute("name") === "paredeUmJanelas") setParedeUm({"altura": paredeUm.altura, "largura": paredeUm.largura, "portas": paredeUm.portas, "janelas": Number(e.target.value)});
      if(e.target.getAttribute("name") === "paredeDoisPortas") setParedeDois({"altura": paredeDois.altura, "largura": paredeDois.largura, "portas": Number(e.target.value), "janelas": paredeDois.janelas});
      if(e.target.getAttribute("name") === "paredeDoisJanelas") setParedeDois({"altura": paredeDois.altura, "largura": paredeDois.largura, "portas": paredeDois.portas, "janelas": Number(e.target.value)});
      if(e.target.getAttribute("name") === "paredeTresPortas") setParedeTres({"altura": paredeTres.altura, "largura": paredeTres.largura, "portas": Number(e.target.value), "janelas": paredeTres.janelas});
      if(e.target.getAttribute("name") === "paredeTresJanelas") setParedeTres({"altura": paredeTres.altura, "largura": paredeTres.largura, "portas": paredeTres.portas, "janelas": Number(e.target.value)});
      if(e.target.getAttribute("name") === "paredeQuatroPortas") setParedeQuatro({"altura": paredeQuatro.altura, "largura": paredeQuatro.largura, "portas": Number(e.target.value), "janelas": paredeQuatro.janelas});
      if(e.target.getAttribute("name") === "paredeQuatroJanelas") setParedeQuatro({"altura": paredeQuatro.altura, "largura": paredeQuatro.largura, "portas": paredeQuatro.portas, "janelas": Number(e.target.value)});

      setCasa([paredeUm, paredeDois, paredeTres, paredeQuatro]);
    } catch(err){
      console.log("Erro ao pegar os dados da casa.")
    }
  }


  async function abrirPopup(e){
    e.preventDefault();
    
    let x = document.getElementById("popupResultado");
    x.className = "mostrar";
  }
  function fecharPopup(){
    let x = document.getElementById("popupResultado");
    x.className = x.className.replace("mostrar", "");
  };


  return(
    <>
    <HomeNavbar/>
    <div className="container">
      <div className="card mt-5 mb-5">
        <div className="card-header">
          <h1>Calculadora de Tinta</h1>
        </div>
        <div className="card-body">
          <form method="post" onSubmit={(e)=> abrirPopup(e)}>

            <label className="mb-3">Insira o tamanho das paredes em centímetros:</label>
            
            <BlocoFormulario>
              
              <div className="parede">
                <span>
                  Parede 1:  
                  <CalculoMetroQuadrado parede={paredeUm} />
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Altura" name="paredeUmAltura" value={paredeUm.altura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Altura</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Largura" name="paredeUmLargura" value={paredeUm.largura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Largura</label>
                </div>
              </div>
              <div className="parede">
                <span>
                  Parede 2:
                  <CalculoMetroQuadrado parede={paredeDois}/>
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Altura" name="paredeDoisAltura" value={paredeDois.altura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Altura</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Largura" name="paredeDoisLargura" value={paredeDois.largura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Largura</label>
                </div>
              </div>
              <div className="parede">
                <span>
                  Parede 3:
                  <CalculoMetroQuadrado parede={paredeTres}/>
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Altura" name="paredeTresAltura" value={paredeTres.altura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Altura</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Largura" name="paredeTresLargura" value={paredeTres.largura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Largura</label>
                </div>
              </div>
              <div className="parede">
                <span>
                  Parede 4:
                  <CalculoMetroQuadrado parede={paredeQuatro}/>
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Altura" name="paredeQuatroAltura" value={paredeQuatro.altura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Altura</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Largura" name="paredeQuatroLargura" value={paredeQuatro.largura} onChange={e => pegarFormulario(e)}/>
                  <label htmlFor="floatingInput">Largura</label>
                </div>
              </div>
              
            </BlocoFormulario>

            <label className="mb-3">Insira a quantidade de portas e janelas por parede:</label>

            <BlocoFormulario>

              <div className="parede">
                <span>Parede 1:
                  <CalculoPortasEJanelas parede={paredeUm} />
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Portas" name="paredeUmPortas" min="0" value={paredeUm.portas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Portas</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Janelas" name="paredeUmJanelas" min="0" value={paredeUm.janelas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Janelas</label>
                </div>
              </div>
              <div className="parede">
                <span>Parede 2:
                  <CalculoPortasEJanelas parede={paredeDois} />
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Portas" name="paredeDoisPortas" min="0" value={paredeDois.portas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Portas</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Janelas" name="paredeDoisJanelas" min="0" value={paredeDois.janelas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Janelas</label>
                </div>
              </div>
              <div className="parede">
                <span>Parede 3:
                  <CalculoPortasEJanelas parede={paredeTres} />
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Portas" name="paredeTresPortas" min="0" value={paredeTres.portas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Portas</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Janelas" name="paredeTresJanelas" min="0" value={paredeTres.janelas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Janelas</label>
                </div>
              </div>
              <div className="parede">
                <span>Parede 4:
                  <CalculoPortasEJanelas parede={paredeQuatro} />
                </span>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Portas" name="paredeQuatroPortas" min="0" value={paredeQuatro.portas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Portas</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="number" className="form-control" id="floatingInput" placeholder="Janelas" name="paredeQuatroJanelas" min="0" value={paredeQuatro.janelas} onChange={e => pegarFormulario(e)} />
                  <label htmlFor="floatingInput">Janelas</label>
                </div>
              </div>
              
            </BlocoFormulario>

            <button type="submit" className="btn btn-primary w-100 fs-4">Calcular</button>

          </form>
        </div>
      </div>
      
      <div id="popupResultado">
        <div>
          <button onClick={fecharPopup}>
          <IoClose size={25}/>
          </button>
          <Resultado casa={casa} />
        </div>
      </div>
    </div>
    <HomeFooter/>
    </>
  )
}