/* eslint-disable */
import logo from "../assets/img/logoTintus.png"

const areaPorta = (80 * 190) / 10000, areaJanela = (200 * 120) / 10000;

export function HomeNavbar(){
  return(
    <nav className="navbar navbar-light bg-primary" style={{height: "90px"}}>
      <div className="container-fluid" style={{marginLeft: "-15px"}}>
        <a className="navbar-brand" href="/">
          <img src={logo} alt="Logo Tintus" style={{width: "130px", position: "relative", bottom: "32px"}} />
        </a>
      </div>
    </nav>
  )
};

export function HomeFooter(){
  return(
    <footer className="w-100 bg-primary text-center" style={{height: "5.2vh", display: "flex", justifyContent: "center", alignItems: "center"}}>
      <span><a href="https://gitlab.com/wesleysantossts" target="_blank" style={{textDecoration: "none", color: "white", fontSize: "14px"}}>Desenvolvido por Wesley Santos</a></span>
    </footer>
  )
}

export function CalculoMetroQuadrado(props){
  const areaParede = Number((props.parede.altura * props.parede.largura) / 10000);

  return(
    <>
      { 
      (areaParede > 0 && areaParede < 1) || areaParede >= 15.0 ?
        <span className="alerta" style={{color: "red", fontWeight: "bold", fontSize: "12px", width: "80%", alignItems: "center", marginLeft: "3px"}}>
          (X) menor: 1m², maior: 15m².
        </span>
        :
        <span className="alerta" style={{color: "blue", fontWeight: "bold", marginLeft: "3px"}}>
          {areaParede > 0 && areaParede.toFixed(1) + " m²"}
        </span>
      }
    </>
  )
};

export function CalculoPortasEJanelas(props){
  const {altura, largura, portas, janelas} = props.parede;
  const parede = { altura: Number(altura), largura: Number(largura), portas: Number(portas), janelas: Number(janelas)};

  const areaParede = (parede.altura * parede.largura) / 1000;
  const totalAreaPortaseJanelas = (parede.portas * areaPorta) + (parede.janelas * areaJanela);

  // O total de área das portas e janelas deve ser no máximo 50% da área de parede
  if((!!parede.altura && !!parede.largura) && totalAreaPortaseJanelas > areaParede / 2){
    return(
      <>
        <span style={{color: "red", fontWeight: "bold", fontSize: "12px", width: "80%", alignItems: "center", marginLeft: "3px"}}>(x) Maior que 50% da parede.</span>
      </>
    )
  };

  // A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
  if(parede.portas !== undefined || parede.portas !== 0){
    const diferencaAlturaParedeePorta = (parede.altura - 190) <= 30; // 190 = altura, em centimetros, da porta.
    
    if(parede.portas !== 0 && diferencaAlturaParedeePorta){
      return(
        <>
          <span style={{color: "red", fontWeight: "bold", fontSize: "12px", width: "80%", alignItems: "center", marginLeft: "3px"}}>Parede muito baixa!</span>
        </>
      )
    };

    return(
      <>
        <span style={{color: "blue", fontWeight: "bold", marginLeft: "3px"}}>{Number(totalAreaPortaseJanelas) !== 0 && Number(totalAreaPortaseJanelas).toFixed(1) + " m²"}</span>
      </>
    )

  };

  return(
    <>
      <span style={{color: "blue", fontWeight: "bold", marginLeft: "3px"}}>0.0 m²</span>
    </>
  );
};

export function Resultado(props){
  //-OK Cada litro rende 5 metros quadrados
  const latasTinta = [0.5, 2.5, 3.6, 18], metrosQdPorLitro = [];
  for (const i in latasTinta) {
    metrosQdPorLitro.push(latasTinta[i] * 5);
  };
  const {0:lataMuitoPequena, 1:lataPequena, 2:lataMedia, 3:lataGrande} = metrosQdPorLitro;
  
  //- OK - Calcular área total da casa
  let totalAreaPorParede = [], totalAreaCasa = {totalParedes: 0, totalPortaseJan: 0};

  if(props.casa[0] !== undefined){
    totalAreaPorParede[0] = {
    areaParedeUm: (Number(props.casa[0].altura) * Number(props.casa[0].largura)) / 10000,
    areaPortaseJanUm: (Number(props.casa[0].portas) * areaPorta) + (Number(props.casa[0].janelas) * areaJanela),
    areaParedeDois: (Number(props.casa[1].altura) * Number(props.casa[1].largura)) / 10000,
    areaPortaseJanDois: (Number(props.casa[1].portas) * areaPorta) + (Number(props.casa[1].janelas) * areaJanela),
    areaParedeTres: (Number(props.casa[2].altura) * Number(props.casa[2].largura)) / 10000,
    areaPortaseJanTres: (Number(props.casa[2].portas) * areaPorta) + (Number(props.casa[2].janelas) * areaJanela),
    areaParedeQuatro: (Number(props.casa[3].altura) * Number(props.casa[3].largura)) / 10000,
    areaPortaseJanQuatro: (Number(props.casa[3].portas) * areaPorta) + (Number(props.casa[3].janelas) * areaJanela)
    };

    const {areaParedeUm, areaParedeDois, areaParedeTres, areaParedeQuatro} = totalAreaPorParede[0];
    const {areaPortaseJanUm, areaPortaseJanDois, areaPortaseJanTres, areaPortaseJanQuatro} = totalAreaPorParede[0];

    totalAreaCasa = {
      totalParedes: (areaParedeUm + areaParedeDois + areaParedeTres + areaParedeQuatro), 
      totalPortaseJan: (areaPortaseJanUm + areaPortaseJanDois + areaPortaseJanTres + areaPortaseJanQuatro)
    };
  };

  let qtdLata = {muitoPequena: 0, pequena: 0, media: 0, grande: 0}, resto = 0, areaPintada = totalAreaCasa.totalParedes - totalAreaCasa.totalPortaseJan;

  //- OK - Fórmulas das latas vendidas
  if(areaPintada >= lataGrande){

    if(areaPintada / lataGrande === 0) qtdLata.grande += (areaPintada / lataGrande);
    
    qtdLata.grande += Math.floor(areaPintada / lataGrande);
    resto += (areaPintada % lataGrande);

    if(resto > 0){
      qtdLata.media += Math.ceil(areaPintada / lataMedia);
      resto += (areaPintada % lataMedia);

      if(resto > 0){
        qtdLata.pequena += Math.ceil(resto / lataPequena);
      }
    }

  } else if(areaPintada >= lataMedia){

    if(areaPintada / lataMedia == 0) qtdLata.media += (areaPintada / lataMedia);

    qtdLata.media += Math.floor(areaPintada / lataMedia);
    resto += (areaPintada % lataMedia);
    
    if(resto > 0){
      qtdLata.pequena += Math.ceil(resto / lataPequena);
      resto += (resto % lataPequena);

      if(resto > 0) qtdLata.muitoPequena += Math.ceil(resto / lataMuitoPequena);
    }

  } else if(areaPintada >= lataPequena){
    
    if(areaPintada / lataPequena === 0) qtdLata.pequena += (areaPintada / lataPequena);

    qtdLata.pequena += Math.floor(areaPintada / lataPequena);
    resto += (areaPintada % lataPequena);

    if(resto > 0){
      qtdLata.muitoPequena += Math.ceil(resto / lataMuitoPequena);
    }
  
  } else if(areaPintada > 0 && areaPintada < lataPequena){
    if(areaPintada / lataMuitoPequena === 0) qtdLata.muitoPequena += (areaPintada / lataMuitoPequena);

    qtdLata.muitoPequena += Math.ceil(areaPintada / lataMuitoPequena);

  } 

  return(
   <div>
    <h2 className="text-center mt-4 mb-3">Resultado</h2>
    <table className="table table-hover w-100">
      <thead>
        <tr>
          <th>Tamanho da tinta</th>
          <th>Quantidade</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{"muito pequena"}</td>
          <td>{qtdLata.muitoPequena}</td>
        </tr>
        <tr>
          <td>{"pequena"}</td>
          <td>{qtdLata.pequena}</td>
        </tr>
        <tr>
          <td>{"média"}</td>
          <td>{qtdLata.media}</td>
        </tr>
        <tr>
          <td>{"grande"}</td>
          <td>{qtdLata.grande}</td>
        </tr>
      </tbody>
    </table>
   </div> 
  )
}