import {createGlobalStyle} from "styled-components";
<link src="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"/>

const GlobalStyle = createGlobalStyle`
  
  * {
    box-sizing: border-box;
  }

  html, body, #root {
    padding: 0;
    margin: 0;
    font-family: "Open Sans", sans-serif;
  }

  button, a {
    cursor: pointer;
    border: none;
  }

  h1, h2, h3 {
    font-weight: 600
  }
`;

export default GlobalStyle;