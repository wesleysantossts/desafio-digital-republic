# Desafio Digital Republic

Aplicação de cálculo de quantidade de tinta necessária para pintar uma área.

## Primeiros passos

<details>
  <summary>Como iniciar este projeto</summary>
  <ul>
    <li>Clone este repositório na sua máquina.</li>
    <li>Entre na pasta <code>client</code> com o comando <code>cd client</code> e depois rode o comando <code>npm install</code> para instalar as dependências.</li>
    <li>Após isso, rode o comando <code>npm start</code> e abrirá a aplicação no seu navegador padrão na porta <code>localhost:3000</code></li>
  </ul>
</details>

## Aplicação

Conforme o objetivo proposto no Code Challenge, a aplicação fornece o cálculo de quantas latas de tinta serão utilizadas para pintar uma área com quatro paredes.

Caso o usuário insira valores que não estejam dentro das seguintes condições aparecerá uma mensagem de erro ao lado do campo da parede que apresentar o erro (conforme mostrado na imagem 01). As condições são:

- Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes.
- O total de área das portas e janelas deve ser no máximo 50% da área de parede.
- A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.

<div align="center">
  <img src="./client/src/assets/img/homeTintus.jpg" width="80%" /><br/>
  <sub>Imagem 01: página inicial da aplicação versão desktop.</sub>
</div><br/>

A aplicação é responsiva, conforme mostrado abaixo.

<div align="center">
  <img src="./client/src/assets/img/homeTintusMobile.png" width="30%"/><br/>
  <sub>Imagem 02: página inicial versão mobile.</sub>
</div><br/>

Assim que o usuário inserir todos os dados, basta clicar no botão <code>Calcular</code> que aparecerá um popup na tela com o resultado, uma tabela informando quantas latas de tinta serão necessárias para pintar a área.

<div align="center">
  <img src="./client/src/assets/img/homeTintusMobilePop.jpg" width="30%"/><br/>
  <sub>Imagem 03: página inicial - popup com resultado do cálculo na versão mobile.</sub>
</div><br/>

## Bibliotecas, Frameworks e Banco de Dados

- [x] React;
- [x] React Router Dom;
- [x] Styled Components;
- [x] Bootstrap.
- [x] SASS.

## Desenvolvimento

<table>
  <tr>
    <td border="1px solid #ddd" align="center">
      <a href="https://github.com/wesleysantossts">
        <img src="https://avatars.githubusercontent.com/u/56703526?v=4" width="100px" alt="Wesley Santos"/>
        <br/>
        <sub>Wesley Santos</sub>
      </a>
    </td>
  </tr>
</table>

## Agradecimentos ✨

Agradeço aos recrutadores e equipe técnica pela oportunidade de fazer o teste e mostrar um pouco do que sei. Isso ajuda muito no meu desenvolvimento e, caso eu seja escolhido, espero que tenhamos uma expansiva e próspera jornada pela frente.
